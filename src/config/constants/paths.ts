export const BASE_PATH = "/";

export const PATHS = {
  HOME_PAGE: BASE_PATH,
  EMPLOYEE_PAGE: BASE_PATH + "employee"
};
