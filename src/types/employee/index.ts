export interface IEmployeeTableProps {}

export interface IEmployeeActionProps {
  employee: IEmployee
}

export interface IEmployeeModalProps {
  employee: IEmployee
  show: boolean;
  onHide: () => void
  updateEmployee: (employee: IEmployee) => void
}

export interface IEmployee {
  id: number;
  name: string;
  email: string;
  isActive?: boolean
}

export type EmployeeState = {
  employees: IEmployee[]
}

export type EmployeeAction = {
  type: string
  employee: IEmployee
}

export type DispatchType = (args: EmployeeAction) => EmployeeAction
