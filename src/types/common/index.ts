import React from 'react'

export interface ITitle {
  variant?:
    | 'h1'
    | 'h2'
    | 'h3'
    | 'h4'
    | 'h5'
    | 'h6'
    | undefined;
  children?: React.ReactNode;
  color?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'danger'
    | 'disabled'
    | 'white'
    | 'black'
    | undefined;
  className?: string;
  bold?: boolean;
  link?: boolean;
  onClick?: () => void;
}

export interface IText {
  variant?:
    | 'p'
    | 'span'
    | undefined;
  children?: React.ReactNode;
  color?:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'danger'
    | 'disabled'
    | 'white'
    | 'black'
    | undefined;
  className?: string;
  bold?: boolean;
  link?: boolean;
  onClick?: () => void;
}

export interface IPaginationProps {
  itemsCount: number,
  itemsPerPage: number,
  currentPage: number,
  setCurrentPage: (value: number) => void,
  alwaysShown: boolean
}

export interface IToggleSwitchProps {
  checked: boolean;
  name: string;
  onChange: (value: boolean) => void
}
