import React from 'react'
import { IToggleSwitchProps } from 'types/common'
import './index.scss'

const ToggleSwitch: React.FC<IToggleSwitchProps> = ({ checked, name, onChange }) => {

  return (
    <div className="toggle-switch">
      <input type="checkbox"
             className="toggle-switch-checkbox"
             checked={checked}
             name={name}
             id={name}
             onChange={e => onChange(e.target.checked)} />
      <label className="toggle-switch-label"
             htmlFor={name}>
        <span className="toggle-switch-inner" />
        <span className="toggle-switch-switch" />
      </label>
    </div>
  )
}

export default ToggleSwitch
