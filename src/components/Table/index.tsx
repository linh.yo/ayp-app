import React, { Children } from 'react'
import { TableProps, Table as ReactTable } from 'react-bootstrap'
import './index.scss'

export interface ITableProps extends TableProps {}

const Table: React.FC<ITableProps> = (props) => {

  return (
    <ReactTable {...props}>
      {Children.toArray(props.children)}
    </ReactTable>
  )
}

export default Table
