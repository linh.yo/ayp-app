import React from 'react'
import { ButtonProps, Button as ReactButton } from 'react-bootstrap'
import './index.scss'

export interface IButtonProps extends ButtonProps {
  color?: string;
  className?: string;
  width?: string;
  height?: string;
  shadow?: boolean;
  backgroundHover?: string;
}

const Button: React.FC<IButtonProps> = (props) => {

  return (
    <ReactButton {...props}
                 className={`${props.className ? props.className : ''} ${props.color ? 'btn-' + props.color : ''}`}>
      {props.children}
    </ReactButton>
  )
}

export default Button
