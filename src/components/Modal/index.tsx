import * as React from 'react';
import {
  ModalProps,
  ModalHeaderProps,
  Modal as ReactModal,
  ModalHeader as ReactModalHeader,
  ModalBody as ReactModalBody,
  ModalTitle as ReactModalTitle
} from 'react-bootstrap'
import './index.scss'

export interface IModalProps extends ModalProps {}
export interface IModalHeaderProps extends ModalHeaderProps {}
export interface IModalTitleProps {}
export interface IModalBodyProps {}

export const Modal: React.FC<IModalProps> = (props) => {
  return (
    <ReactModal{...props}>
      {props.children}
    </ReactModal>
  )
}

export const ModalHeader: React.FC<IModalHeaderProps> = (props) => {
  return (
    <ReactModalHeader {...props}>
      {props.children}
    </ReactModalHeader>
  )
}

export const ModalBody: React.FC<IModalBodyProps> = (props) => {
  return (
    <ReactModalBody {...props}>
      {props.children}
    </ReactModalBody>
  )
}

export const ModalTitle: React.FC<IModalTitleProps> = (props) => {
  return (
    <ReactModalTitle {...props}>
      {props.children}
    </ReactModalTitle>
  )
}
