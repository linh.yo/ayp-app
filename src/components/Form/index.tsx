import * as React from 'react';
import {
  FormProps,
  FormGroupProps,
  FormControlProps,
  Form as ReactForm,
  FormGroup as ReactFormGroup,
  FormLabel as ReactFormLabel,
  FormControl as ReactFormControl,
  FormText as ReactFormText,
} from 'react-bootstrap'
import './index.scss'

export interface IFormProps extends FormProps {}
export interface IFormGroupProps extends FormGroupProps {
  className?: string;
}
export interface IFormControlProps extends FormControlProps {
  name?: string;
  label?: string;
  required?: boolean;
  register?: any;
}
export interface IFormLabelProps {
  className?: string;
}
export interface IFormTextProps {
  className?: string;
}

export const Form: React.FC<IFormProps> = (props) => {
  return (
    <ReactForm{...props}>
      {props.children}
    </ReactForm>
  )
}

export const FormGroup: React.FC<IFormGroupProps> = (props) => {
  return (
    <ReactFormGroup {...props} className={props?.className}>
      {props.children}
    </ReactFormGroup>
  )
}

export const FormLabel: React.FC<IFormLabelProps> = (props) => {
  return (
    <ReactFormLabel {...props} className={props?.className}>
      {props.children}
    </ReactFormLabel>
  )
}

export const FormText: React.FC<IFormTextProps> = (props) => {
  return (
    <ReactFormText {...props} className={props?.className}>
      {props.children}
    </ReactFormText>
  )
}

export const FormControl: React.FC<IFormControlProps> = (props) => {
  return (
    <ReactFormControl {...props} />
  )
}
