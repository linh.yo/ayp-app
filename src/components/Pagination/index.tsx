import React, { useEffect } from 'react'
import { Pagination as ReactPagination } from 'react-bootstrap'
import { IPaginationProps } from 'types/common'
import { scrollToTop } from 'utils/hooks/useScroll'
import './index.scss'

const Pagination: React.FC<IPaginationProps> = ({
  itemsCount,
  itemsPerPage,
  currentPage,
  setCurrentPage,
  alwaysShown
}) => {

  const pagesCount = Math.ceil(itemsCount / itemsPerPage)
  const isPaginationShown = alwaysShown ? true : pagesCount > 1
  const isCurrentPageFirst = currentPage === 1
  const isCurrentPageLast = currentPage === pagesCount

  const changePage = (number: number) => {
    if (currentPage === number) return;
    setCurrentPage(number);
    scrollToTop();
  };

  const onPageNumberClick = (pageNumber: number) => {
    changePage(pageNumber);
  };

  const onPreviousPageClick = () => {
    changePage(currentPage - 1)
  };

  const onNextPageClick = () => {
    changePage(currentPage + 1)
  };

  const setLastPageAsCurrent = () => {
    if (currentPage > pagesCount) {
      setCurrentPage(pagesCount)
    }
  };

  let isPageNumberOutOfRange: boolean

  const pageNumbers = [...new Array(pagesCount)].map((_, index) => {
    const pageNumber = index + 1;
    const isPageNumberFirst = pageNumber === 1;
    const isPageNumberLast = pageNumber === pagesCount;
    const isCurrentPageWithinTwoPageNumbers =
      Math.abs(pageNumber - currentPage) <= 2;

    if (
      isPageNumberFirst ||
      isPageNumberLast ||
      isCurrentPageWithinTwoPageNumbers
    ) {
      isPageNumberOutOfRange = false;
      return (
        <ReactPagination.Item
          key={pageNumber}
          onClick={() => onPageNumberClick(pageNumber)}
          active={pageNumber === currentPage}
        >
          {pageNumber}
        </ReactPagination.Item>
      )
    }

    if (!isPageNumberOutOfRange) {
      isPageNumberOutOfRange = true;
      return <ReactPagination.Ellipsis key={pageNumber} className="muted" />
    }

    return null
  })

  useEffect(setLastPageAsCurrent, [pagesCount])

  return (
    <>
      {isPaginationShown && (
        <ReactPagination>
          <ReactPagination.Prev onClick={onPreviousPageClick}
                                disabled={isCurrentPageFirst}
          />
          {pageNumbers}
          <ReactPagination.Next onClick={onNextPageClick}
                                disabled={isCurrentPageLast}
          />
        </ReactPagination>
      )}
    </>
  )
}

export default Pagination
