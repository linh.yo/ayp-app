import React from 'react';
import './App.css';
import 'assets/styles/main.scss'
import AppRoute from './routes';

function App() {
  return (
    <AppRoute />
  );
}

export default App;
