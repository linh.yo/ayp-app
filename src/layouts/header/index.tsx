import React, { useState } from 'react'
import { Nav, Navbar, Collapse, NavbarToggler, NavbarBrand, NavItem } from 'reactstrap'
import Logo from 'assets/images/AYP-logo.webp'
import { PATHS } from 'config/constants/paths'
import { NavLink } from 'react-router-dom'
import './index.scss'

const Header = () => {
  const [collapsed, setCollapsed] = useState<boolean>(true)
  const toggleNavbar = () => setCollapsed(!collapsed)

  return (
    <header id="header">
      <Navbar container
              expand="md"
              fixed="top"
              light>
        <NavbarBrand href={PATHS.HOME_PAGE}>
          <img src={Logo} alt="AYP" />
        </NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink to={PATHS.HOME_PAGE} className={({ isActive }) =>
                isActive ? 'nav-link active' : 'nav-link'
              }>
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink to={PATHS.EMPLOYEE_PAGE} className={({ isActive }) =>
                isActive ? 'nav-link active' : 'nav-link'
              }>
                Employee
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </header>
  );
};

export default Header;
