import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { facebook, linkedIn, youtube } from '../../components/Icon'
import Title from 'components/Title'
import Text from 'components/Text'
import './index.scss'

const Footer = () => {

  return (
    <footer id="footer">
      <Container>
        <Row className="align-items-center">
          <Col xs={12} sm={4}>
            <Title variant="h5">
              Leading the Future of Work
            </Title>
          </Col>
          <Col xs={12} sm={4} className="text-center">
            <Text>
              Copyright © 2022, AYP Group.All rights reserved.
            </Text>
          </Col>
          <Col xs={12} sm={4}>
            <div className="d-flex align-items-center justify-content-center justify-content-md-end">
              <Title variant="h6" className="me-3">
                Connect With Us
              </Title>
              <ul>
                <li>
                  <a href="https://www.facebook.com/AYPGroupSG/"
                     target="_blank" rel="noreferrer">
                    {facebook}
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/company/ayp-associates-pte-ltd"
                     target="_blank" rel="noreferrer">
                    {linkedIn}
                  </a>
                </li>
                <li>
                  <a href="https://www.youtube.com/channel/UChUoLB0wpWC-QNSelf90hwA?view_as=subscriber"
                     target="_blank" rel="noreferrer">
                    {youtube}
                  </a>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
