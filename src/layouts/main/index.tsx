import React from 'react'
import Header from 'layouts/header'
import Footer from 'layouts/footer'

type Props = {
  children: JSX.Element | JSX.Element[]
};

const MainLayout = ({ children }: Props) => {

  return (
    <React.Fragment>
      <Header />
      <main>
        {children}
      </main>
      <Footer />
    </React.Fragment>
  );
};

export default MainLayout;
