import React, { ReactElement } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomePage from 'pages/HomePage';
import { PATHS } from 'config/constants/paths'
import MainLayout from 'layouts/main'
import EmployeePage from 'pages/EmployeePage'

const AppRoute: React.FC = (): ReactElement => {
  return (
    <Router>
      <MainLayout>
        <Routes>
          <Route path={PATHS.HOME_PAGE} element={<HomePage />} />
          <Route path={PATHS.EMPLOYEE_PAGE} element={<EmployeePage />} />
        </Routes>
      </MainLayout>
    </Router>
  )
};

export default AppRoute
