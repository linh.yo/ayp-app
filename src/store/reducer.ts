import * as actionTypes from "./actionTypes"
import { EmployeeAction, EmployeeState, IEmployee } from 'types/employee'
import Data from './employees.json'

const initialState: EmployeeState = {
  employees: Data?.employees
}

const reducer = (
  state: EmployeeState = initialState,
  action: EmployeeAction
): EmployeeState => {
  switch (action.type) {
    case actionTypes.ADD_EMPLOYEE:
      const newEmployee: IEmployee = {
        id: Math.random(),
        name: action.employee.name,
        email: action.employee.email,
        isActive: action.employee.isActive
      }
      return {
        ...state,
        employees: state.employees.concat(newEmployee),
      }
    case actionTypes.UPDATE_EMPLOYEE:
      const updatedEmployees_: IEmployee[] = state.employees.map(employee => {
          if (employee.id === action.employee.id) {
            return action.employee
          } else {
            return employee
          }
        }
      )
      return {
        ...state,
        employees: updatedEmployees_
      }
    case actionTypes.REMOVE_EMPLOYEE:
      const updatedEmployees: IEmployee[] = state.employees.filter(
        employee => employee.id !== action.employee.id
      )
      return {
        ...state,
        employees: updatedEmployees,
      }
  }
  return state
}

export default reducer
