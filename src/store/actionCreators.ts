import * as actionTypes from "./actionTypes"
import { IEmployee, EmployeeAction, DispatchType } from 'types/employee'

export function addEmployee(employee: IEmployee) {
  const action: EmployeeAction = {
    type: actionTypes.ADD_EMPLOYEE,
    employee,
  }

  return simulateHttpRequest(action)
}

export function updateEmployee(employee: IEmployee) {
  const action: EmployeeAction = {
    type: actionTypes.UPDATE_EMPLOYEE,
    employee,
  }

  return simulateHttpRequest(action)
}

export function removeEmployee(employee: IEmployee) {
  const action: EmployeeAction = {
    type: actionTypes.REMOVE_EMPLOYEE,
    employee,
  }
  return simulateHttpRequest(action)
}

export function simulateHttpRequest(action: EmployeeAction) {
  return (dispatch: DispatchType) => {
    setTimeout(() => {
      dispatch(action)
    }, 500)
  }
}
