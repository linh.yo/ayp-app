import React from 'react'
import { useForm, Controller } from 'react-hook-form'
import { IEmployee, IEmployeeModalProps } from 'types/employee'
import { close } from 'components/Icon'
import { Spinner } from 'reactstrap'
import { useLoading } from 'utils/hooks/useLoading'
import Button from 'components/Button'
import { Modal, ModalHeader, ModalBody, ModalTitle } from 'components/Modal'
import { Form, FormGroup, FormLabel, FormText, FormControl } from 'components/Form'
import { PATTERN_VALIDATE } from 'config/constants/pattern'
import { Dispatch } from 'redux'
import { useDispatch } from 'react-redux'
import ToggleSwitch from 'components/ToggleSwitch'

const EmployeeModal: React.FC<IEmployeeModalProps> = ({
  employee,
  show,
  onHide,
  updateEmployee
}) => {

  const [{isLoading}, {start, stop}] = useLoading()
  const { control, errors, handleSubmit } = useForm()
  const dispatch: Dispatch<any> = useDispatch()


  const onSubmit = (values: IEmployee) => {
    start()
    const data = {
      id: employee?.id,
      name: values?.name,
      email: values?.email,
      isActive: values?.isActive
    }
    dispatch(updateEmployee(data))
    onHide()
  }

  return (
    <Modal centered
           show={show}
           onHide={onHide}
           backdrop="static"
           dialogClassName="employee-modal">
      <a className="modal-close" onClick={onHide}>{close}</a>
      <ModalHeader>
        <ModalTitle>{employee?.id ? 'Update' : 'Create'} Employee</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <Form className="employee-form">
          <FormGroup className="form-group">
            <FormLabel>
              Name
            </FormLabel>
            <Controller control={control}
                        defaultValue={employee?.name}
                        name="name"
                        rules={{
                          required: "Field is required",
                        }}
                        render={({ onChange, onBlur, value, name }) => (
                <FormControl type="text"
                             placeholder="Employee Name"
                             onChange={onChange}
                             onBlur={onBlur}
                             value={value}
                             name={name}
                />
              )}
            />
            {errors.name && (
              <FormText className="text-danger">
                {errors.name.message}
              </FormText>
            )}
          </FormGroup>
          <FormGroup className="form-group">
            <FormLabel>
              Email
            </FormLabel>
            <Controller control={control}
                        defaultValue={employee?.email}
                        name="email"
                        rules={{
                          required: "Field is required",
                          pattern: PATTERN_VALIDATE.email
                        }}
                        render={({ onChange, onBlur, value, name }) => (
                <FormControl type="email"
                             placeholder="Employee Email"
                             onChange={onChange}
                             onBlur={onBlur}
                             value={value}
                             name={name}
                />
              )}
            />
            {errors.email && (
              <FormText className="text-danger">
                {errors.email.message}
              </FormText>
            )}
          </FormGroup>
          <FormGroup className="form-group">
            <FormLabel className="me-3">
              Status
            </FormLabel>
            <Controller control={control}
                        defaultValue={employee?.isActive}
                        name="isActive"
                        render={({ onChange, value, name }) => (
                          <ToggleSwitch checked={value}
                                        onChange={onChange}
                                        name={name} />
                        )}
            />
          </FormGroup>
        </Form>

        <div className="form-submit mt-4 text-center">
          <Button type="button"
                  variant="light"
                  onClick={onHide}
                  className="me-3">
            Cancel
          </Button>
          <Button type="submit"
                  variant="primary"
                  onClick={handleSubmit(onSubmit)}>
            {
              isLoading ? <Spinner size="sm"/> : 'Save'
            }
          </Button>
        </div>
      </ModalBody>
    </Modal>
  )
};

export default EmployeeModal
