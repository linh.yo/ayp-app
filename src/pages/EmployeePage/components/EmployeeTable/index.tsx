import React, { useEffect, useState } from 'react'
import {EmployeeState, IEmployee, IEmployeeTableProps} from 'types/employee'
import { EMPLOYEE_STATUS } from 'config/constants/enum'
import Table from 'components/Table'
import Pagination from 'components/Pagination'
import EmployeeAction from '../EmployeeAction'
import { useSelector, shallowEqual } from 'react-redux'

const EmployeeTable: React.FC<IEmployeeTableProps> = () => {

  const [employees, setEmployees] = useState<Array<IEmployee>>([])
  const [pageCount, setPageCount] = useState<number>(1)
  const [pageSize, setPageSize] = useState<number>(5)
  const [currentPage, setCurrentPage] = useState<number>(1)

  const updatedEmployees: IEmployee[] = useSelector(
    (state: EmployeeState) => state.employees,
    shallowEqual
  )

  useEffect(() => {
    setEmployees(updatedEmployees?.slice((currentPage - 1) * pageSize, currentPage * pageSize))
    setPageCount(updatedEmployees?.length)
  }, [currentPage, updatedEmployees])

  // Get the list of employees
  // const getData = () => {
  //   fetch('employees.json', {
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Accept': 'application/json'
  //       }
  //     }
  //   )
  //     .then((response) => {
  //       return response.json()
  //     })
  //     .then((data) => {
  //       setEmployees(data?.employees?.slice((currentPage - 1) * pageSize, currentPage * pageSize))
  //       setPageCount(data?.employees?.length)
  //     })
  // }

  // Get the list of employees when changing page number
  // useEffect(() => {
  //   getData()
  // }, [currentPage])

  return (
    <section className="employee-table">
      <Table striped bordered>
        <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Email</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        {
          employees.map((item, index) => (
            <tr key={index}>
              <td>{item.id}</td>
              <td>{item.name}</td>
              <td>
                <a href={`mailto:${item.email}`}>
                  {item.email}
                </a>
              </td>
              <td>
                {item.isActive ? EMPLOYEE_STATUS.ACTIVE : EMPLOYEE_STATUS.DEACTIVATED}
              </td>
              <td>
                {
                  item.isActive &&
                  <EmployeeAction employee={item} />
                }
              </td>
            </tr>
          ))
        }
        </tbody>
      </Table>

      <Pagination itemsCount={pageCount}
                  itemsPerPage={pageSize}
                  currentPage={currentPage}
                  setCurrentPage={setCurrentPage}
                  alwaysShown={true}
      />
    </section>
  )
}

export default EmployeeTable
