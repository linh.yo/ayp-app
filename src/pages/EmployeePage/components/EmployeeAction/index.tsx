import React, { useState } from 'react'
import Button from 'components/Button'
import EmployeeModal from '../EmployeeModal'
import { IEmployeeActionProps } from 'types/employee'
import { updateEmployee } from 'store/actionCreators'

const EmployeeAction: React.FC<IEmployeeActionProps> = ({ employee }) => {
  const [showUpdateModal, setShowUpdateModal] = useState<boolean>(false)

  return (
    <section className="employee-action">
      <Button onClick={() => setShowUpdateModal(true)}>
        Update
      </Button>
      {
        showUpdateModal &&
          <EmployeeModal employee={employee}
                         show={showUpdateModal}
                         onHide={() => setShowUpdateModal(false)}
                         updateEmployee={updateEmployee} />
      }
    </section>
  )
}

export default EmployeeAction
