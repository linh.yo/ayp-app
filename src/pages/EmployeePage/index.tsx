import React from 'react'
import Title from 'components/Title'
import { Container, Row, Col } from 'react-bootstrap'
import EmployeeSearch from './components/EmployeeSearch'
import EmployeeTable from './components/EmployeeTable'
import './index.scss'

const EmployeePage = () => {
  return (
    <article className="employee-page pt-5 pb-5">
      <Container>
        <Row className="mb-4">
          <Col xs={12} sm={6}>
            <Title variant="h1" className="page-title">Employee</Title>
          </Col>
          <Col xs={12} sm={6}>
            <EmployeeSearch />
          </Col>
        </Row>

        <EmployeeTable />
      </Container>
    </article>
  )
}

export default EmployeePage
