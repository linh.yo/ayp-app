import React from 'react'
import Title from 'components/Title'
import Text from 'components/Text'
import { Container } from 'react-bootstrap'
import './index.scss'
import Button from 'components/Button'
import { useNavigate } from 'react-router-dom'
import { PATHS } from 'config/constants/paths'

const HomePage = () => {

  const navigate = useNavigate()

  const goToEmployeePage = () => {
    navigate(PATHS.EMPLOYEE_PAGE)
  }

  return (
    <article className="home-page pt-5 pb-5">
      <Container>
        <Title variant="h1" className="page-title">
          Welcome to AYP App !
        </Title>
        <Text className="mt-4 mb-2">
          AYP Group is a leading HR technology solutions provider headquartered in Singapore, with regional offices across Asia Pacific.
        </Text>
        <Text className="mb-5">
          We believe in making digital workplaces happier and smarter through technology, helping companies to build a scalable and engaged hybrid workforce.
        </Text>

        <Button onClick={goToEmployeePage}>
          Explore more
        </Button>
      </Container>
    </article>
  )
}

export default HomePage
